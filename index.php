<?php
require 'Slim/Slim.php';

use \Slim\Slim;

Slim::registerAutoloader();

// Instantiate a slim application
$app = new Slim();

// Define HTTP routes
$app->post('/operations', 'addOperation');
$app->post('/currencies', 'addCurrency');
$app->post('/currencies', 'addCurrency');

$app->get('/currencies', 'getCurrencies');

$app->get('/operations/latest', 'getLatestOperation');
$app->get('/wallet', 'getWallet');
$app->get('/wallet/before', 'getWalletBefore');

$app->put('/currencies/:currency', 'updateCurrency');


// Run the Slim application
$app->run();

//return amount of the last operation
function getLastAmountBDD(){
    $dom = new DomDocument;
    $dom->load("data.xml");
    $lastOpAmount = $dom->getElementsByTagName('lastamount');
    return trim($lastOpAmount->item(0)->nodeValue);
}

//return "currency" of the last operation
function getLastcurrencyBDD(){
    $dom = new DomDocument;
    $dom->load("data.xml");
    $lastOpAmount = $dom->getElementsByTagName('currency');
    return trim($lastOpAmount->item(0)->nodeValue);
}

//return last type of the last operation
function getLastTypeBDD(){
    $dom = new DomDocument;
    $dom->load("data.xml");
    $lastOpType = $dom->getElementsByTagName('type');
    return trim($lastOpType->item(0)->nodeValue);
}

/** 
 * Gets an operation for the wallet and adds it to the wallet (accounting entry and payment)
 */
function addOperation() {  
    global $app;

    $request = Slim::getInstance()->request();
    $operation = $request->getBody();

    setOperation($operation);
}

/** 
 * set an operation to the wallet (accounting entry and payment)
 */
function setOperation($operation) {
    global $app;
    $operation = json_decode($operation);

    $type = $operation->{'type'};
    $amount = $operation->{'amount'};  
    $currency = $operation->{'currency'};


    if (!is_numeric($amount)){ //test if it's a numeric value in the json amount
        $error = array(
            'error' => '400 : le type de données n\'est pas valide'
        );
        $app->response()->status(400);
        echo json_encode($error);
    }else{
        $dom = new DomDocument;
        $dom->load("data.xml");

        $allMoneyInPocket= $dom->getElementsByTagName('amount');

        $lastType = $dom->getElementsByTagName('type')->item(0);

        $lastAmount = $dom->getElementsByTagName('lastamount')->item(0);

        foreach($allMoneyInPocket as $item){
            if ($item->getAttribute('currency')==$currency){
                switch($type){
                    case 'payment':
                        if (($item->nodeValue)<$amount){
                            $error = array(
                                'error' => '500 : la quantité d\'argent est insuffisante'
                            );
                            $app->response()->status(500);
                            echo json_encode($error);
                        }else{
                            $item->nodeValue -= $amount; //decrease the value of the total money contained in the wallet
                            $lastType->nodeValue = $type;
                            $lastAmount->nodeValue = $amount;

                        }
                        break;
                    case 'entry':
                        $item->nodeValue += $amount; //increase the value of the total money contained in the wallet
                        $lastType->nodeValue = $type;
                        $lastAmount->nodeValue = $amount;
                        break;
                    case 'change':
                        $to = $operation->{'to'};

                        /*if (($item->nodeValue)<$amount){
                            $error = array(
                                'error' => '500 : la quantité d\'argent est insuffisante'
                            );
                            $app->response()->status(500);
                            echo json_encode($error);
                        }else{*/
                            $item->nodeValue -= $amount; //decrease the value of the total money contained in the wallet
                            $taux = $item->nodeValue;
                            foreach($allMoneyInPocket as $item2){
                                if ($item2->getAttribute('currency')==$to){
                                    $item2->nodeValue = $amount*((int)getTaux($to)/getTaux($currency));
                                }
                            }

                        /*}*/
                        break;
                }
            }
        }

        $dom->save('data.xml');
        echo(json_encode($operation));

    }
}


/**
 * Sends the latest operation made on the wallet
 */
function getLatestOperation() {
    global $app;

    if (getLastTypeBDD() != null){
        // json creation of lasOp
        $lastOpJSON = array(
            'type' => getLastTypeBDD(), 
            'amount' => getLastAmountBDD(),
            'currency' => getLastcurrencyBDD()
        );
        echo json_encode($lastOpJSON);
    }
}

/**
 * Sends the wallet's present state
 */
function getWallet() {
    global $app;

    // json creation of wallet
    $dom = new DomDocument;
    $dom->load("data.xml");
    
    $lastOpType = $dom->getElementsByTagName('amount');
    
    $wallet = array();
    foreach($lastOpType as $item){
        $wallet[$item->getAttribute('currency')] = $item->nodeValue;
    }
    echo json_encode($wallet);
}

/**
 * Sends actual currencies in the database
 */
function getCurrencies() {
    global $app;

    // json creation of currencies listing
    $dom = new DomDocument;
    $dom->load("data.xml");
    
    $currenciesData = $dom->getElementsByTagName('rate');
    
    $currencies = array();
    foreach($currenciesData as $item){
        $currency = array();
        $currency['currency'] = $item->getAttribute('currency');
        $currency['rate'] = $item->nodeValue;
        $currencies [] = $currency;
    }
    echo json_encode($currencies);
}

/**
 * Sends the previous wallet's state
 */
function getWalletBefore() {
    global $app;

    // json creation of wallet
    $dom = new DomDocument;
    $dom->load("data.xml");
    
    $allMoneyInPocket = $dom->getElementsByTagName('amount');
    
    $wallet = array();
    foreach($allMoneyInPocket as $item){
        switch (getLastTypeBDD()) {
            case 'payment':
                if (getLastcurrencyBDD() == $item->getAttribute('currency')){
                    $wallet[$item->getAttribute('currency')] = $item->nodeValue+getLastAmountBDD();
                }else{
                    $wallet[$item->getAttribute('currency')] = $item->nodeValue;
                }
                break;
            case 'entry':
                if (getLastcurrencyBDD() == $item->getAttribute('currency')){
                    $wallet[$item->getAttribute('currency')] = $item->nodeValue-getLastAmountBDD();
                }else{
                    $wallet[$item->getAttribute('currency')] = $item->nodeValue;
                }
                break;
        }
    }
    echo json_encode($wallet);
}


/**
 * Add new currency to database
 */
function addCurrency() {

    $request = Slim::getInstance()->request();
    $curr = $request->getBody();

    setCurrency($curr);
}

function setCurrency($json){
    global $app;

    $currency = json_decode($json);

    $name = $currency->{'currency'};
    $taux = $currency->{'rate'}; 

    if (!is_numeric($taux)){ //test if it's a numeric value in the json amount
        $error = array(
            'error' => '400 : le type de données n\'est pas valide'
        );
        $app->response()->status(400);
        echo json_encode($error);
    }else{
        $dom = new DomDocument;
        $dom->load("data.xml");

        //récupération du noeud père du xml
        $moneyNode = $dom->getElementsByTagName('money');

        //création du noeud à insérer
        $newCurrency = $dom->createElement('rate');
        $newCurrency->setAttribute("currency",$name);
        $newCurrency->nodeValue = $taux;

        //ajout du noeud aux enfants
        $moneyNode->item(0)->appendChild($newCurrency);

        //récupération du noeud wallet
        $walletNode = $dom->getElementsByTagName('wallet'); 

        //création du noeud à insérer
        $newWalletPocket = $dom->createElement('amount');
        $newWalletPocket->setAttribute("currency",$name);
        $newWalletPocket->nodeValue = 0;

        //ajout du noeud aux enfants de wallet
        $walletNode->item(0)->appendChild($newWalletPocket);

        //save file
        $dom->save('data.xml');
        echo(json_encode($currency));
    }

}

function updateCurrency($currency){
    global $app;
    $request = Slim::getInstance()->request();
    $curr = $request->getBody();
    $devise = json_decode($curr);

    $taux = $devise->{'rate'}; 
    $found = false;

    if (!is_numeric($taux)){ //test if it's a numeric value in the json amount
        $error = array(
            'error' => '400 : le type de données n\'est pas valide'
        );
        $app->response()->status(400);
        echo json_encode($error);
    }else{
        $dom = new DomDocument;
        $dom->load("data.xml");
        //recuperation des devises de la bdd
        $currenciesData = $dom->getElementsByTagName('rate');

        foreach($currenciesData as $item){
            if ($item->getAttribute('currency') == $currency){
                $item->nodeValue = $taux;
                $found = true;
            }
        }

        if($found == false){
            $error = array(
                'error' => '404 : currency not found'
            );
            $app->response()->status(404);
            echo json_encode($error);
        }else{
            echo json_encode($devise);
        }
        //save file
        $dom->save('data.xml');
    }
}

/**
 * return actual rate of $devise
 */
function getTaux($devise) {

    // json creation of currencies listing
    $dom = new DomDocument;
    $dom->load("data.xml");
    
    $currenciesData = $dom->getElementsByTagName('rate');
    
    foreach($currenciesData as $item){

        if (($item->getAttribute('currency'))==$devise){
            return $item->nodeValue;
        }
    }
}