# Fonctionnalités par itération

## Iteration 1

* **ajout d'argent**
    * **400** : la quantité d'argent n'est pas valide
    * **500** : le porte monnaie est trop plein (variable long dépassée)
* **paiement (retrait d'argent)**
    * **400** : la quantité d'argent n'est pas valide
    * **500** : la quantité d'argent est insuffisante

## Iteration 2
* **possède des devises prédéfinies**
    * EUR : 1
    * GBP : 0.84
    * SEK : 8.69
    * DEK : 1.33
* **ajout de devises avec taux de change**
    * **400** : le nom de la devise n'est pas renseigné
    * **400** : le taux de change n'est pas valide

## Iteration 3

* **ajout d'argent dans n'importe quelle devise existante**
    * **400** : la quantité d'argent n'est pas valide
    * **400** : le nom de la devise n'est pas renseigné
    * **400** : la devise n'existe pas
    * **500** : le porte monnaie est trop plein (variable long dépassée)
* **paiement dans toutes les devises existantes**
    * **400** : la quantité d'argent n'est pas valide
    * **400** : le nom de la devise n'est pas renseigné
    * **400** : la devise n'existe pas
    * **500** : la quantité d'argent est insuffisante
* **changer une partie de l'argent d'une devise en une autre devise**
    * **400** : le nom de la devise n'est pas renseigné
    * **400** : la quantité d'argent n'est pas valide
    * **500** : la quantité d'argent est insuffisante

## Iteration 4 (best iteration ever)

* **utilisation d'une librairie pour manipuler les montants avec des variables plus grandes que les long**