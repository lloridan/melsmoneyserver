<?php

require("index.php");

class indexTest extends PHPUnit_Framework_TestCase
{
    /*Teste si la fonction retourne bien un entier*/
    public function testGetWalletNumeric()
    {
        $test = true;
        ob_start();
        getWallet();
        $ret =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        foreach ($ret as $key => $value) {
            if(is_numeric($value)){
                if(!ctype_digit($value)){
                    $test = false;
                }
            }else{
                $test = false;
            }
        }
        $this->assertTrue($test);
    }

    /*Teste si la fonction retourne bien des valeurs positives ou nulles*/
    public function testGetWalletPositive()
    {
        $test = true;
        ob_start();
        getWallet();
        $ret =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        foreach ($ret as $key => $value) {
            if($value < 0){
                $test = false;
            }
        }
        $this->assertTrue($test);
    }

    /*Teste si la fonction retourne bien un entier*/
    public function testGetLastAmountBDD()
    {
        $ret = getLastAmountBDD();
        if(is_numeric($ret)){
            $this->assertTrue(ctype_digit($ret));
        }else{
            $this->assertTrue(false);
        }
    }

    /*Teste si le type de la dernière operation est correct*/
    public function testGetLastTypeBDD()
    {
        $ret = getLastTypeBDD();
        if($ret == "payment" || $ret == "entry" || $ret == null){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

    /*Teste si la structure du json est correcte*/
    public function testGetLatestOperation()
    {
        ob_start();
        getLatestOperation();
        $ret =  json_decode(ob_get_contents(), true);
        $this->assertTrue(array_key_exists("amount", $ret) && array_key_exists("type", $ret) && array_key_exists("currency", $ret));
    }

    /*Teste si la structure du json est correcte*/
    public function testGetWallet()
    {
        ob_start();
        getWallet();
        $ret =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        $test = true;

        $dom = new DomDocument;
        $dom->load("data.xml");
        $currencies = $dom->getElementsByTagName("rate");

        foreach($currencies as $item){
            $curr = $item->getAttribute("currency");
            if(!array_key_exists($curr, $ret)){
                $test = false;
            }
        } 

        foreach ($ret as $key => $value) {
            if(!ctype_digit($value)){
                $test = false;
            }
        }
        $this->assertTrue($test);
    }

    /*Teste si la dernière operation est correcte*/
    public function testGetWalletBefore()
    {
        ob_start();
        getWalletBefore();
        $ret =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        $test = false;
        $key = "";

        $dom = new DomDocument;
        $dom->load("data.xml");
        $currencies = $dom->getElementsByTagName("rate");

        foreach($currencies as $item){
            $curr = $item->getAttribute("currency");
            if(array_key_exists($curr, $ret)){
                $key = $curr;
            }
        }

        if($key != ""){
            if(ctype_digit($ret[$key])){
                $test = true;
            }
        }

        $this->assertTrue($test);
    }

    /* Verifie si l'operation s'effectue correctement*/
    public function testSetOperation(){
        ob_start();
        getLatestOperation();
        $lastOperation = ob_get_contents();
        ob_end_flush();

        ob_start();
        getWallet();
        $currentAmount =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        $curr = getLastcurrencyBDD();
        $solde = $currentAmount[$curr];

        $resultatCorrect = true;
        $array = json_decode($lastOperation, true);
        if($array["type"] == "payment"){
            $nouvelleOperation = array(
                'type' => 'entry', 
                'amount' => $array["amount"],
                'currency' => curr()
            );
            setOperation(json_encode($nouvelleOperation));
            setOperation(json_encode($nouvelleOperation));
            $nouvelleOperation = array(
                'type' => 'payment', 
                'amount' => $array["amount"],
                'currency' => curr()
            );
            setOperation(json_encode($nouvelleOperation));

            ob_start();
            getWallet();
            $newAmount =  json_decode(ob_get_contents(), true);
            ob_end_flush();

            $newSolde = $newAmount[$curr];
            if($solde != $newSolde){
                $resultatCorrect = false;
            }
        }else if($array["type"] == "entry"){
            $nouvelleOperation = array(
                'type' => 'payment', 
                'amount' => $array["amount"],
                'currency' => $curr
            );
            setOperation(json_encode($nouvelleOperation));
            setOperation(json_encode($nouvelleOperation));
            $nouvelleOperation = array(
                'type' => 'entry', 
                'amount' => $array["amount"],
                'currency' => $curr
            );
            setOperation(json_encode($nouvelleOperation));

            ob_start();
            getWallet();
            $newAmount =  json_decode(ob_get_contents(), true);
            ob_end_flush();

            $newSolde = $newAmount[$curr];
            if($solde != $newSolde){
                $resultatCorrect = false;
            }
        }else{
            $resultatCorrect = false;
        }
        $this->assertTrue($resultatCorrect);
    }

    /* Verifie s'il y a bien un erreur lorsqu'il n'y a pas assez pour effectuer l'operation*/
    public function testSetOperationImpossibleMinimum(){
        $curr = getLastcurrencyBDD();

        ob_start();
        getWallet();
        $json =  json_decode(ob_get_contents(), true);
        ob_end_flush();

        $solde = $json[$curr];



        $currentIntAmount = intval($solde);
        
        $newAmount = ($currentIntAmount + 10)."";

        $nouvelleOperation = array(
            'type' => 'payment', 
            'amount' => $newAmount,
            'currency' => getLastcurrencyBDD()
        );

        ob_start();
        setOperation(json_encode($nouvelleOperation));
        $ret = ob_get_contents();
        ob_end_flush();

        $array = json_decode($ret, true);
        $this->assertTrue(array_key_exists("error", $array));
    }

    /* Verifie s'il y a bien un erreur lorsqu'il n'y a pas assez pour effectuer l'operation*/
    public function testSetOperationImpossibleType(){
        $nouvelleOperation = array(
            'type' => 'payment', 
            'amount' => "aaaaa",
            'currency' => getLastcurrencyBDD()
        );

        ob_start();
        setOperation(json_encode($nouvelleOperation));
        $ret = ob_get_contents();
        ob_end_flush();

        $array = json_decode($ret, true);
        $this->assertTrue(array_key_exists("error", $array));
    }

    /* Verifie s'il y a bien un erreur lorsqu'il n'y a pas assez pour effectuer l'operation*/
    public function testGetLastcurrencyBDD(){
        $ret = getLastcurrencyBDD();

        $valTest = false;

        $dom = new DomDocument;
        $dom->load("data.xml");
        $currencies = $dom->getElementsByTagName('rate');

        foreach($currencies as $item){
            $curr = $item->getAttribute('currency');
            if($curr == $ret){
                $valTest = true;
            }
        } 
        $this->assertTrue($valTest);
    }

    public function testSetCurrency(){
        $newCurrency = array(
            'currency' => 'OOO', 
            'rate' => "aaaaa"
        );

        ob_start();
        setCurrency(json_encode($newCurrency));
        $ret = ob_get_contents();
        ob_end_flush();

        $array = json_decode($ret, true);
        $this->assertTrue(array_key_exists("error", $array));

    }
}
?>